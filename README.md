# Test-Habi

Deploy Api with gitlab


- Install dependencies

```sh
$ docker run --rm --entrypoint '' --workdir /repo -v $(pwd):/repo python:3.8-slim-buster pip3 install --target build --ignore-installed --no-deps --requirement src/requirements.txt
```

- Build project

```sh
$ docker run --rm --entrypoint '' --workdir /repo -v $(pwd):/repo alpine mkdir -p dist
$ docker run --rm --entrypoint '' --workdir /repo -v $(pwd):/repo alpine sh -c 'apk add --update zip && cd build && zip -gr9 ../dist/main.zip .'
$ docker run --rm --entrypoint '' --workdir /repo -v $(pwd):/repo alpine sh -c 'apk add --update zip && zip -gr9 dist/main.zip src'
```
- Update lambda code

```sh
$ aws lambda update-function-code \
 --function-name test-pro-upload-image \
 --zip-file fileb://dist/main.zip \
 --publish \
 --output json
```

- Update lambda config

```sh
$ aws lambda update-function-configuration \
 --function-name test-pro-upload-image \
 --environment "Variables={NODE_ENV=dev}" 
```

- Test cors api gateway
 
```sh
curl -v -X OPTIONS https://jxq7rhpsbe.execute-api.us-east-2.amazonaws.com/test/upload-images
```
 
- Update Api Gateway Code
 
```sh
$ aws apigateway put-rest-api \
--rest-api-id jxq7rhpsbe \
--cli-binary-format raw-in-base64-out \
--mode merge \
--no-fail-on-warnings \
--body fileb://apidocs.json
```
 
- Update deployment Api Gateway
 
```sh
$ aws apigateway create-deployment \
--rest-api-id jxq7rhpsbe \
--stage-name test \
--stage-description 'Deployment api 10' 
```
